import React from 'react';
import './styles.css';

function Header() {
    return (
        <div className="content-header">
          <header>
            <nav>
              <div className="logo">
                <a href="#project"><img className="photo" src="/logoS.png" alt="logo"/></a>
                <text className="header-title">Dose</text>
              </div>
              <div className="menu-list">
                <ul>
                  <li><a href="#project">Project</a></li>
                  <li><a href="#features">Features</a></li>
                  <li><a href="#stories">Stories</a></li>
                  <li><a href="#form-content">Contact us</a></li>
                </ul>
              </div>
            </nav>
          </header>
        </div>
    );
}

export default Header;