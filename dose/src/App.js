import './App.css';
import { LandingPage, Header } from './screen';

function App() {
  return (
    <div className="App">
      <Header/>
      <body>
        <LandingPage/>
      </body>
    </div>
  );
}

export default App;
