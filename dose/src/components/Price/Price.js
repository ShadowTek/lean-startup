import React from 'react';
import './styles.css';

function Price () {
    return (
        <div id="price-content">
            <h2>About price</h2>
            <div className="container">
                <div className="free">
                <img src="/free.svg" alt="Free" width="100px"/>
                <h3>Free account</h3>
                <ul>
                    <li>Find the place to practice your sport</li>
                    <li>Create a maximum of one team per sport</li>
                    <li>Check the occupancy rate of public places</li>
                    <li>Save a maximum of 2 sports in your profile</li>
                </ul>
                </div>
                <div className="premium">
                <img src="/premium.svg" alt="Premium" width="100px"/>
                <h3>Premium account</h3>
                <ul>
                    <li>All features of Free account</li>
                    <li>No team limit</li>
                    <li>No sport limit</li>
                </ul>
                <div className="price">
                    <p className="text-price">For 0.99€ / month</p>
                </div>
                </div>
            </div>
        </div>
    );
}

export default Price;