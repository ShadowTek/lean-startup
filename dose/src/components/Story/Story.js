import React from 'react';
import './styles.css';

function Story() {
    return (
        <div className="story-wrapper">
            <text className="title">They talk about us</text>
            <div className="story-content">
                <div className="story1">
                    <text className="content">
                        Hugo - 22 years - Paris<br/><br/>
                        I used to do sport with my friends on my weekend, but since some of them got injured,
                        we can't do a match with a full team. With my friends, we use the application to find
                        trustable people to play with.<br/><br/>
                    </text>
                </div>
                <div className="story2">
                    <text className="content">
                        Lucile - 20 years - Toulouse<br/><br/>
                        I'm new in town and i don't know where to go to do sport.
                        A friend show me this application and since i use it to find a secure
                        place to practice my favorite sport.<br/><br/>
                    </text>
                </div>
                <div className="story3">
                    <text className="content">
                        Julien - 25 years - Paris<br/><br/>
                        With my friends we used to go in public spot to play football, but most of the time
                        the place was full, so we couldn't play. Now we use Dose to find
                        place not overcrowded and if we don't, we can create one on a public spot
                        It allow us to met new people almost on each session.
                    </text>
                </div>
            </div>
        </div>
    );
}

export default Story;