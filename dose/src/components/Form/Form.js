import React from 'react';
import './styles.css';

function Form () {
    return (
        <div id="form-content">
            <h2>Join our Newsletter</h2>
            <p id="text">The project is under development. If you want to be informed of the progress of
                the project, subscribe to our Newsletter.
            </p>
            <form className="form">
                <input className="mail-input" type="text" name="mail" placeholder="Email adress"/>
                <button className="submit-button">Subscribe</button>
            </form>
        </div>
    );
}

export default Form;